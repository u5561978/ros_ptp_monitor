#!/usr/bin/env python

import os
import rospy
import diagnostic_updater
import diagnostic_msgs


class PtpMonitor:

    def __init__(self):
        rospy.init_node('ptp_monitor')
        self.diagnostics = diagnostic_updater.Updater()
        self.diagnostics.setHardwareID('none')
        task = diagnostic_updater.CompositeDiagnosticTask("PTP")
        task.addTask(diagnostic_updater.FunctionDiagnosticTask("PTP Stats", self.update_stats))
        task.addTask(diagnostic_updater.FunctionDiagnosticTask("Clock sync check", self.check_sync_bounds))
        task.addTask(diagnostic_updater.FunctionDiagnosticTask("Clock correction check", self.check_correction_bounds))
        wifi_task = diagnostic_updater.FunctionDiagnosticTask("Wifi Status", self.update_wifi)
        self.diagnostics.add(task)
        self.diagnostics.add(wifi_task)
        self.rate = rospy.Rate(0.5)
        self.ptp_file = "/var/run/ptpd2.status"
        self.wifi_file = "/proc/net/wireless"

        self.port_state = ''
        self.offset = 0
        self.clock_correction = 0

    def check_sync_bounds(self, stat):
        if abs(self.offset) > 0.01:
            stat.summary(diagnostic_msgs.msg.DiagnosticStatus.WARN, "Time is not synced")
        elif abs(self.offset) > 0.005:
            stat.summary(diagnostic_msgs.msg.DiagnosticStatus.WARN, "Time sync is inaccurate")
        else:
            stat.summary(diagnostic_msgs.msg.DiagnosticStatus.OK, "Time sync is within tolerance")
        return stat

    def check_correction_bounds(self, stat):
        if abs(self.clock_correction) > 400:
            stat.summary(diagnostic_msgs.msg.DiagnosticStatus.WARN, "Clock correction is high")
        else:
            stat.summary(diagnostic_msgs.msg.DiagnosticStatus.OK, "Clock correction is within tolerance")

        return stat

    def get_ptp_data(self):
        with open(self.ptp_file, 'r') as f:
            lines = f.readlines()
            data = dict(map(line_to_map, lines))
            return data

    def update_stats(self, stat):
        if os.path.exists(self.ptp_file):
            data = self.get_ptp_data()
            
            stat.add('Local Time', data['Local time'])
            stat.add('Port State', data['Port state'])
            stat.add('Master IP', data.get('Best master IP', ''))
            stat.add('Offset from Master', data.get('Offset from Master', ''))
            stat.add('Clock Correction', data.get('Clock correction', ''))
            stat.add('Path Delay', data.get('Mean Path Delay', ''))

            if data['Port state'] == 'PTP_SLAVE':
                self.port_state = data['Port state']
                self.offset = float(data.get('Offset from Master', '0').split(' ')[0])
                self.clock_correction = float(data.get('Clock correction', '0').split(' ')[0])
                stat.summary(diagnostic_msgs.msg.DiagnosticStatus.OK, "PTP is connected")
            else:
                stat.summary(diagnostic_msgs.msg.DiagnosticStatus.WARN, "PTPD is not connected to master")
        else:
            rospy.loginfo("PTPD is not running")
            stat.summary(diagnostic_msgs.msg.DiagnosticStatus.WARN, "PTPD is not running")

        return stat

    def get_wifi_data(self):
        with open(self.wifi_file, 'r') as f:
            lines = f.readlines()
            if len(lines) > 2:
                data = lines[2].split()
                return data
            else:
                return None

    def update_wifi(self, stat):
        if os.path.exists(self.wifi_file):
            data = self.get_wifi_data()
            if data is not None:
                print(data)
                stat.add('Link Quality', data[2])
                stat.add('Link Level', data[3])
                stat.add('Link Noise', data[4])
                stat.summary(diagnostic_msgs.msg.DiagnosticStatus.OK, "Wifi Connected")
            else:
                stat.summary(diagnostic_msgs.msg.DiagnosticStatus.ERROR, "Wifi Not Connected")
        else:
            stat.summary(diagnostic_msgs.msg.DiagnosticStatus.ERROR, "Unknown Wifi Status")
        return stat

            

    def run(self):
        self.diagnostics.update()
        self.rate.sleep()


def line_to_map(line):
    key_val = line.split(':', 1)
    key = key_val[0].strip()
    val = key_val[1].strip()
    return key, val


if __name__ == "__main__":
    monitor = PtpMonitor()
    while not rospy.is_shutdown():
        monitor.run()
